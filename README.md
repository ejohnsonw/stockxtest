# True to size webservice

To run, please clone the repository

## With Docker
```
docker-compose up --build -d
```
###
to stop
```
docker-compose down
```
## Endpoints


### Add Sneaker
```

POST http://localhost:4232/addSneaker  

{"objectID":"d0a19f59-1f0d-41b4-83a6-08b920f22b45","name":"adidas Yeezy Boost 350 V2 Blue Tint"}
```


### Retrieve Sneakers
```
GET http://localhost:4232/
```



### Add Data Point
```
POST http://localhost:4232/addDataPoint 

{"objectID":"3774e829-99f4-46d6-bb94-f9e19ad55ad3","sneakerSize":2}
```

### Get Average with objectID
```
GET http://localhost:4232/trueToSizeCalculation/3774e829-99f4-46d6-bb94-f9e19ad55ad3
```

