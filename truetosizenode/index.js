const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 4232

const Pool = require('pg').Pool
const pool = new Pool({
    user: 'postgres',
    host: 'postgres',
    database: 'sneakertestnode',
    password: 'mysecretpassword',
    port: 5432,
})

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

function findSneaker(objectID, callback) {
    pool.query('SELECT * FROM sneaker where objectid = $1', [objectID], (error, results) => {
        if (error) {
            throw error
        }
        callback(results.rows)
    })
}

function findDataPointsForSneaker(sneakerId, callback) {
    pool.query('SELECT * FROM true_to_size_data where sneaker_id = $1', [sneakerId], (error, results) => {
        callback(results.rows)
    })
}


app.get('/', (request, response) => {
    pool.query('SELECT * FROM sneaker ORDER BY name ASC', (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
})

app.post('/addSneaker', (request, response) => {
    const {
        objectID,
        name
    } = request.body
    findSneaker(objectID, function(rows) {
        if (rows.length == 0) {
            pool.query('INSERT INTO sneaker (objectID,name) VALUES ($1, $2)', [objectID, name], (error, results) => {
                if (error) {
                    throw error
                }
                response.status(201).send(request.body)
            })
        } else {
            response.status(200).json(rows)
        }
    })

})

app.post('/addDataPoint', (request, response) => {
    const {
        objectID,
        sneakerSize
    } = request.body

    findSneaker(objectID, function(rows) {
        if (rows.length == 1) {
            if (sneakerSize >= 1 && sneakerSize <= 5) {
                pool.query('INSERT INTO true_to_size_data (measurement,sneaker_id) VALUES ($1,$2)', [sneakerSize, rows[0]["id"], ], (error, results) => {
                    if (error) {
                        response.status(500).json({
                            "error": "Could not save measurement"
                        })
                        return
                    }
                    response.status(201).send(request.body)
                    return
                })
            } else {
                response.status(500).json({
                    "error": "Invalid Size reported"
                })
            }
        } else {
            response.status(500).json({
                "error": "Product does not exist"
            })
            return
        }
    })
})


app.get('/trueToSizeCalculation/:objectID', (request, response) => {
    findSneaker(request.params.objectID, function(rows) {
        if (typeof rows[0] != 'undefined') {
            var sneakerId = rows[0]["id"]

            findDataPointsForSneaker(sneakerId, function(rowsMeasurements) {
                if (rowsMeasurements.length >= 1) {
                    var total = 0
                    rowsMeasurements.forEach(function(dataPoint) {
                        total += dataPoint.measurement
                    })
                    var average = total / rowsMeasurements.length
                    response.status(200).json({
                        "trueToSizeCalculation": average
                    })
                } else {
                    response.status(500).json({
                        "error": "No data points for this product "
                    })
                    return
                }
            })
        } else {
            response.status(500).json({
                "error": "Product does not exist"
            })
        }

    })
})



app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})