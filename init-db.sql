--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1 (Debian 11.1-1.pgdg90+1)
-- Dumped by pg_dump version 11.1 (Debian 11.1-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: sneaker; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sneaker (
    id bigint NOT NULL,
    objectid character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.sneaker OWNER TO postgres;

--
-- Name: sneaker_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sneaker_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sneaker_id_seq OWNER TO postgres;

--
-- Name: sneaker_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sneaker_id_seq OWNED BY public.sneaker.id;


--
-- Name: true_to_size_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.true_to_size_data (
    id bigint NOT NULL,
    sneaker_id bigint NOT NULL,
    measurement integer NOT NULL
);


ALTER TABLE public.true_to_size_data OWNER TO postgres;

--
-- Name: true_to_size_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.true_to_size_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.true_to_size_data_id_seq OWNER TO postgres;

--
-- Name: true_to_size_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.true_to_size_data_id_seq OWNED BY public.true_to_size_data.id;


--
-- Name: sneaker id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sneaker ALTER COLUMN id SET DEFAULT nextval('public.sneaker_id_seq'::regclass);


--
-- Name: true_to_size_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.true_to_size_data ALTER COLUMN id SET DEFAULT nextval('public.true_to_size_data_id_seq'::regclass);


--
-- Data for Name: sneaker; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sneaker (id, objectid, name) FROM stdin;
1	3774e829-99f4-46d6-bb94-f9e19ad55ad3	adidas Yeezy Boost 350 V2 Sesame
2	d16d221d-f7f0-4f00-895c-0f4009abc223	adidas Yeezy Boost 350 V2 Semi Frozen Yellow
\.


--
-- Data for Name: true_to_size_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.true_to_size_data (id, sneaker_id, measurement) FROM stdin;
11	1	1
12	1	2
13	1	2
14	1	3
15	1	2
16	1	3
17	1	2
18	1	2
19	1	2
20	1	2
21	1	3
22	1	3
23	1	4
24	1	5
25	1	2
\.


--
-- Name: sneaker_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sneaker_id_seq', 2, true);


--
-- Name: true_to_size_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.true_to_size_data_id_seq', 25, true);


--
-- Name: sneaker sneaker_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sneaker
    ADD CONSTRAINT sneaker_pkey PRIMARY KEY (id);


--
-- Name: true_to_size_data true_to_size_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.true_to_size_data
    ADD CONSTRAINT true_to_size_data_pkey PRIMARY KEY (id);


--
-- Name: true_to_size_data fk23cqkt2ru09bx3vbom2sbeb3d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.true_to_size_data
    ADD CONSTRAINT fk23cqkt2ru09bx3vbom2sbeb3d FOREIGN KEY (sneaker_id) REFERENCES public.sneaker(id);


--
-- PostgreSQL database dump complete
--

